
const shapes = ['ratón', 'libro', 'sillón', 'fantasma', 'botella']
const colors = {
  ratón: 'gris',
  libro: 'azul',
  sillón: 'rojo',
  fantasma: 'blanco',
  botella: 'verde',
}

export { shapes, colors }

import { defineStore } from 'pinia'
import shuffle from '@/lib/shuffle.js'
import card_builder from '@/lib/card_builder.js'
import { shapes } from '@/const/'


export const store = defineStore('store', {
  state: () => ({
    state: 'ready',
    score: 0,
    cards: [],
    solution: '',
  }),
  getters: {
    foo: () => {},
  },
  actions: {
    newGame(){
      this.state = 'started'
      this.score = 0
      this.reset()
    },
    reset() {
      const cards = shuffle([...shapes])
      this.solution = cards[0]
      this.cards = card_builder(cards)
    },
    answer(item) {
      if(item == this.solution){
        this.score += 1
        this.reset()
      } else {
        this.state = 'lost'
      }
    }
  },
})

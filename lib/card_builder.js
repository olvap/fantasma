import shuffle from '@/lib/shuffle.js'
import { shapes, colors } from '@/const/'
//given an array of cards this function returns a object that represent
//the card in the board

function logicA(cards) {
  return [
    {
      shape: cards[1],
      color: colors[cards[2]],
    },
    {
      shape: cards[3],
      color: colors[cards[4]],
    },
  ]
}
function logicB(cards) {
  return shuffle([
    {
      shape: cards[0],
      color: colors[cards[0]],
    },
    {
      shape: cards[1],
      color: colors[cards[2]],
    },
  ])
}

export default function(cards) {
  return shuffle([logicA, logicB])[0](cards)
}

